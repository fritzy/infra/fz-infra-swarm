#!/bin/sh
export AUTH_PASSWORD=admin
export TRAEFIK_WILDCARD_DOMAIN=infra.localhost

ansible-galaxy install -r requirements.yml
ansible-playbook -i inventories/localhost swarm-on-localhost.yml
