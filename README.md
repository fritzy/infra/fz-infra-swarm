# Fritzy infra swarm

Contains these swarm services used in the infrastructure:

- dockersocket used to reach the docker socket on each node
- portainer as a swarm cluster visualizer
- traefik as a reverse proxy
- swarmprom as a monitoring system

## Overview

### Cluster visualisation

```plantuml
@startuml
cloud loadbalancer

node manager1 {
  agent dockersocket.1 [
    dockersocket
  ]
  agent portaineragent.1 [
    portainer-agent
  ]
  component traefik.1 [
    traefik.1
  ]
  component portainer
}
node manager2 {
  agent dockersocket.2 [
    dockersocket
  ]
  agent portaineragent.2 [
    portainer-agent
  ]
  agent alertmanager
  agent grafana
  agent prometheus
  agent unsee
}
node manager3 {
  agent dockersocket.3 [
    dockersocket
  ]
  agent portaineragent.3 [
    portainer-agent
  ]
  component traefik.2 [
    traefik.2
  ]
}
node worker1 {
  agent portaineragent.4 [
    portainer-agent
  ]
}
node worker2 {
  agent portaineragent.5 [
    portainer-agent
  ]
}

loadbalancer --> traefik.1 : manager-1:443
loadbalancer -> traefik.2 : manager-2:443
loadbalancer .> manager2 : manager-3:443

portainer -> portaineragent.1
portainer ---> portaineragent.2
portainer ---> portaineragent.3
portainer -d-> portaineragent.4
portainer -d-> portaineragent.5

traefik.1 -> dockersocket.1
traefik.2 -> dockersocket.3

@enduml
```

### Docker networks

- docker-socket used containers to connect to docker socket
- infra-public used to connect containers to outside world throught traefik rules
- monitoring_internal used in swarmprom internal communication

```plantuml
@startuml
cloud loadbalancer

card infra-public {
  agent infra_traefik.2 [
    infra_traefik
  ]
  agent infra_portainer.2 [
    infra_portainer
  ]
  agent monitoring_alertmanager.2 [
    monitoring_alertmanager
  ]
  agent monitoring_grafana.2 [
    monitoring_grafana
  ]
  agent monitoring_prometheus.2 [
    monitoring_prometheus
  ]
  agent monitoring_unsee.2 [
    monitoring_unsee
  ]
}

loadbalancer -> infra_traefik.2 : https://<service>.example.com
infra_traefik.2 -> monitoring_alertmanager.2
infra_traefik.2 --> infra_portainer.2
infra_traefik.2 --> monitoring_grafana.2
infra_traefik.2 --> monitoring_prometheus.2
infra_traefik.2 --> monitoring_unsee.2

@enduml
```

```plantuml
@startuml
card docker-socket {
  agent infra_dockersocket
  agent infra_portaineragent [
    infra_portainer-agent
  ]
  agent infra_traefik.1 [
    infra_traefik
  ]
  agent infra_portainer.1 [
    infra_portainer
  ]
}

infra_traefik.1 -r-> infra_dockersocket
infra_portainer.1 -r-> infra_portaineragent
infra_traefik.1 -[hidden]- infra_portainer.1
@enduml
```

```plantuml
@startuml
card monitoring_internal {
  agent monitoring_alertmanager.1 [
    monitoring_alertmanager
  ]
  agent monitoring_cadvisor
  agent monitoring_dockerdexporter [
    monitoring_dockerd-exporter
  ]
  agent monitoring_grafana.1 [
    monitoring_grafana
  ]
  agent monitoring_nodeexporter [
    monitoring_node-exporter
  ]
  agent monitoring_prometheus.1 [
    monitoring_prometheus
  ]
  agent monitoring_unsee.1 [
    monitoring_unsee
  ]
}

monitoring_unsee.1 -d-> monitoring_alertmanager.1
monitoring_grafana.1 -d-> monitoring_prometheus.1
monitoring_prometheus.1 --> monitoring_cadvisor
monitoring_prometheus.1 --> monitoring_dockerdexporter
monitoring_prometheus.1 --> monitoring_nodeexporter
@enduml
```

### Swarm stacks

- infra contains service used for the infrastructure
- monitoring contains swarm monitoring services

```plantuml
@startuml
card infra {
  agent infra_dockersocket
  agent infra_portaineragent [
    infra_portainer-agent
  ]
  agent infra_portainer
  agent infra_traefik
}
@enduml
```

```plantuml
@startuml
card monitoring {
  agent monitoring_alertmanager
  agent monitoring_cadvisor
  agent "monitoring_dockerd-exporter"
  agent monitoring_grafana
  agent "monitoring_node-exporter"
  agent monitoring_prometheus
  agent monitoring_unsee
}
@enduml
```

## common environment variables used to deploy

- DEPLOY_HOSTNAME_IDENTITY=**mandatory** *ansible ssh private key*
- HCLOUD_TOKEN=**mandatory** *hetzner api token*

## Portainer

Portainer will be deployed as swarm service *infra_portainer*.

```plantuml
@startuml
cloud loadbalancer

card swarm_cluster {

  node portainer.1

  folder gluster [
    /mnt/ha/portainer
    ---
    Portainer database
  ]
}

loadbalancer -> portainer.1 : PORTAINER_DOMAIN:443
portainer.1 -> gluster
@enduml
```

### Portainer access configuration

- PORTAINER_DOMAIN=**portainer.{{TRAEFIK_WILDCARD_DOMAIN}} if not specified** *portainer url*

## Swarmprom

swarmprom will be deployed as swarm services *monitoring_alertmanager / monitoring_grafana / monitoring_prometheus / monitoring_unsee*.

- alertmanager (alerts dispatcher) *alertmanager.SWARMPROM_DOMAIN:443* protected by traefik basic auth
- grafana (visualize metrics) *grafana.SWARMPROM_DOMAIN:443*
- prometheus (metrics database) *prometheus.SWARMPROM_DOMAIN:443* protected by traefik basic auth
- unsee (alert manager dashboard) *unsee.SWARMPROM_DOMAIN:443* protected by traefik basic auth

- cadvisor (containers metrics collector)
- dockerd-exporter (Docker daemon metrics collector)
- node-exporter (host metrics collector)

```plantuml
@startuml
cloud loadbalancer

card swarm_cluster {
  node alertmanager.1
  node grafana.1
  node prometheus.1
  node unsee.1

  
  folder glustera [
    /mnt/ha/alertmanager
    ---
    Alertmanager files
  ]

  folder glusterg [
    /mnt/ha/grafana
    ---
    Grafana database
  ]

  folder glusterp [
    /mnt/ha/prometheus
    ---
    Prometheus database
  ]
}

loadbalancer --> alertmanager.1 : alertmanager.SWARMPROM_DOMAIN:443
loadbalancer --> grafana.1 : grafana.SWARMPROM_DOMAIN:443
loadbalancer --> prometheus.1 : prometheus.SWARMPROM_DOMAIN:443
loadbalancer --> unsee.1 : unsee.SWARMPROM_DOMAIN:443

alertmanager.1 -d-> glustera
grafana.1 -d-> glusterg
prometheus.1 -d-> glusterp
@enduml
```

### Swarmprom access configuration

- GRAFANA_ADMIN_USER=**admin if not specified** *grafana default admin user*
- GRAFANA_ADMIN_PASSWORD=**admin if not specified** *grafana default admin password*
- SWARMPROM_DOMAIN=**{{TRAEFIK_WILDCARD_DOMAIN}} if not specified** *swarmprom url*

## Traefik

Traefik will be deployed as swarm service *infra_traefik*. two replicas on two different managers will provide service high availability.

```plantuml
@startuml
cloud loadbalancer

card swarm_cluster {

  node traefik.1
  node traefik.2

  folder gluster as "/mnt/ha/traefik/certs" {
    file acme [
      acme.json
      ---
      Let's encrypt certificates
    ]
  }
}

traefik.1 -> gluster
traefik.2 --> gluster
loadbalancer -> traefik.1 : TRAEFIK_DOMAIN:443
loadbalancer ---> traefik.2 : TRAEFIK_DOMAIN:443
@enduml
```

### Dashboard access configuration

- AUTH_USERNAME=**admin if not specified** *Basic auth username*

- AUTH_PASSWORD=**mandatory [either AUTH_PASSWORD or AUTH_HASHED_PASSWORD]** *Basic auth password, will be hashed to bcrypt*
- AUTH_HASHED_PASSWORD=**$(openssl passwd -apr1)** *Basic auth hashed password. Can be md5 / sha1 / bcrypt*
- AUTHORIZED_IPS=**optional** *AUTHORIZED IPS CIDR for infra and monitoring containers*

- TRAEFIK_DOMAIN=**traefik.{{TRAEFIK_WILDCARD_DOMAIN}} if not specified** *Traefik dashboard url*

### Traefik access configuration

- TRAEFIK_WILDCARD_DOMAIN=**example.com** *Traefik wilcard domain use in portainer / traefik / swarmprom

- LETS_ENCRYPT_DNS_PROVIDER=**mandatory cloudflare** *Lets encrypt provider used in dns challenge*
- LETS_ENCRYPT_ENVIRONMENT_VARIABLES=**{"CF_API_EMAIL": "admin@exmple.com", "CF_API_KEY": "key"}** *Lets encrypt provider variables as a json object*
- LETS_ENCRYPT_EMAIL=**mandatory admin@exmple.com** *Lets encrypt email used*
